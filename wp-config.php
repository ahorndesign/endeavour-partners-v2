<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'endeavour_v2');

/** MySQL database username */
define('DB_USER', 'endeavour');

/** MySQL database password */
define('DB_PASSWORD', 'endeavour12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z<dwcRk:]S*(<Pb0+IF,.[o8++H*R%VIk`k:-+x.<QgXvdWwBh|d)xZ{Qd%e}e7u');
define('SECURE_AUTH_KEY',  ' 1(IHqpVI[Bl#+fkp0tm>08f=t3)iynQ3PM9]<YGf+^kxz-*y&}YGU+RxFL-$2<J');
define('LOGGED_IN_KEY',    'kYPV|trm|bw(<U}|~ZPeHB~6.XGkh`1+[XO;q@vcGW:YDq;*z Mi$n$ic(}z-B/q');
define('NONCE_KEY',        'syKoRzmdiX~FhL59A*QysDTR?^gK~@C@PjNvx|KFEd@8fL?UGnA+&w^`AIo%V>f3');
define('AUTH_SALT',        'LqFjl8)w}Yt@((jF%HdU;2_v?foTenk4&e|X~?g,WMpw*+>a;JXjO}zx`)MD&,{M');
define('SECURE_AUTH_SALT', 'h0>{#,sO`&^L+MB~~d;v3,Kv`|&rBQN+BrLun@L(m+ym$(#=S+uIvLw,f(zR{J@A');
define('LOGGED_IN_SALT',   'YhjL=Kp=EvR:AM*qJ2Cc`[W]:mO#OVC#/KO>6^>gdC:YFg5LXScvj4ODPx!V#JHh');
define('NONCE_SALT',       'ovaXCUM~o|nw80us+Yk+U+n84?Rz=Cqf(C_fgs!2yoLcPgA4P^.8i2V:*1$mmxe3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'enddev_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
